package com.vitasoft.tech.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact,Serializable> {
	
//	For Soft delete based on activatewSW
//	public Contact findByActivateSW(String activateSW);

}
